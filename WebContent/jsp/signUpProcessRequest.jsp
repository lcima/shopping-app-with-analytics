<%@page import="java.util.*, org.json.simple.JSONObject, helpers.*"%>

<%
	String name = request.getParameter("name");
	String role = request.getParameter("role");
	Integer age = null;
	try {
		age = Integer.parseInt(request.getParameter("age"));
	} catch (NumberFormatException e) {
	}
	String state = request.getParameter("state");
%>

<%
	JSONObject result = new JSONObject();
	boolean nameValidationResult = helpers.SignupHelper
			.validateName(name);

	if (nameValidationResult) {
		result.put("signUpValidationResult", "invalidName");
	} else {
		boolean signUpValidationResult = helpers.SignupHelper.signup(
				name, age, role, state);

		if (signUpValidationResult) {
			result.put("signUpValidationResult", "signUpSuccess");
		} else {
			result.put("signUpValidationResult", "signUpFailure");
		}
	}

	out.print(result);
	out.flush();
%>