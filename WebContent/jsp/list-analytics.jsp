<%@page import="java.util.*" import="helpers.*" import="models.*"%>
<%
	String action = request.getParameter("action");
	String category = request.getParameter("category");

	if (action != null && action.equals("run")) {
		if (category == null) {
			out.println(HelperUtils
					.printError("Bad request. Please try again."));
		} else {
			Analytics analytics;
			analytics = AnalyticsHelper.AnalysisForStates("", category,
					0, 0, out);
			/* new code */
			if (analytics.getAccessDBTime() != null) {
				session.setAttribute("updateTime",
						analytics.getAccessDBTime());
			}
			String catStr = (category.equals("0")) ? "All" : category;
			String result = "Searching ";
			result += "category=\"" + catStr + "\"";
			result += ",time=" + analytics.getTime_taken() + "ms)";
			out.println(HelperUtils.printSuccess(result));
			int size = 50 - analytics.getProducts().size();
			int[][] prices = analytics.getPrices();
			int i = 0, j = 0;
%>
<table class="table table-bordered" align="center">
	<thead>
		<tr align="center">
			<th class="col-md-1">
				<B></B>
			</th>
			<%
				for (Integer pid : analytics.getProductPositions()) {
							Integer price = analytics.getProductPrices().get(pid);
							price = (price != null) ? price : 0;
			%>
			<th class="col-md-1">
				<div class="display" id="<%=pid + ": productname"%>" style="color: black">
					<B><%=analytics.getProducts().get(pid).getName()%></B>
				</div>
				<div class="display" id="<%=pid + ": productamount"%>" style="color: black">
					<%=price%>
				</div>
			</th>
			<%
				}
			%>
			<%
				for (int k = 0; k < size; k++) {
			%>
			<th class="col-md-1">
				<B></B>
			</th>
			<%
				}
			%>
		</tr>
	</thead>
	<tbody>
		<%
			for (Integer sid : analytics.getStatePositions()) {
						Integer price = analytics.getStatePrices().get(sid);
						price = (price != null) ? price : 0;
						System.out.println("name: "
								+ analytics.getStates().get(sid).getName()
								+ "; id: " + sid);
		%>
		<tr align="center">
			<td class="col-md-1">
				<div class="display" id="<%=sid + ": statename"%>" style="color: black">
					<B><%=analytics.getStates().get(sid).getName()%></B>
				</div>
				<div class="display" id="<%=sid + ": stateamount"%>" style="color: black">
					<%=price%>
				</div>
			</td>
			<%
				for (Integer pid : analytics.getProductPositions()) {
			%>
			<td class="col-md-1">
				<div class="display" id="<%=sid + ", " + pid%>" style="color: black">
					<%=prices[i][j]%>
				</div>
			</td>
			<%
				j++;
							}
			%>
			<%
				for (int k = 0; k < size; k++) {
			%>
			<td class="col-md-1"></td>
			<%
				}
			%>
		</tr>
		<%
			i++;
						j = 0;
					}
		%>
	</tbody>
</table>
</div>
</div>
<%
	}
	}
%>