<%-- new code --%>
<%@page
	import="java.util.*, org.json.simple.JSONObject, org.json.simple.JSONArray, helpers.*"
	import="models.*"%>

<%
	String category = request.getParameter("category");
	String updatetime = (String) session.getAttribute("updateTime");

	Pair<JSONArray, String> result_pair = AnalyticsHelper
			.AnalysisForStatesRefresh(updatetime, category, out);
	session.setAttribute("updateTime", result_pair.getRight());

	JSONArray result_json = result_pair.getLeft();
	System.out.print(result_json);
	System.out.print("sending back data to AJAX");

	out.print(result_json);
	out.flush();
%>