/* new code */
function validate() {
	var params = new Array();
	var name = document.getElementById("name").value;
	var role = document.getElementById("role").value;
	var age = document.getElementById("age").value;
	var state = document.getElementById("state").value;

	var submitOK = "true";
	var nameValidation = "";
	var signUpValidation = "";

	if (name == "") {
		document.getElementById("nameError").style.display = "block";
		submitOK = "false";
	} else {
		document.getElementById("nameError").style.display = "none";
	}

	if (role == "Role") {
		document.getElementById("roleError").style.display = "block";
		submitOK = "false";
	} else {
		document.getElementById("roleError").style.display = "none";
	}

	if (age.length == 0) {
		document.getElementById("ageError").style.display = "block";
		document.getElementById("ageError").innerHTML = "Age not provide!";
		submitOK = "false";
	} else if (isNaN(age)) {
		document.getElementById("ageError").style.display = "block";
		document.getElementById("ageError").innerHTML = "Please enter a positive integer value!";
		submitOK = "false";
	} else {
		var x = parseInt(age);
		if (x < 0) {
			document.getElementById("ageError").style.display = "block";
			document.getElementById("ageError").innerHTML = "Please enter a positive integer value!";
			submitOK = "false";
		} else {
			document.getElementById("ageError").style.display = "none";
		}
	}

	if (state == "State") {
		document.getElementById("stateError").style.display = "block";
		submitOK = "false";
	} else {
		document.getElementById("stateError").style.display = "none";
	}

	params.push("name=" + name);
	params.push("role=" + role);
	params.push("age=" + age);
	params.push("state=" + state);

	if (submitOK == "true") {
		validateSignUp(params);
	} else {
		document.getElementById("invalidName").style.display = "none";
		document.getElementById("signUpSuccess").style.display = "none";
		document.getElementById("signUpFailure").style.display = "none";
	}
}

function validateSignUp(params, action) {
	$
			.ajax({
				type : 'POST',
				url : "jsp/signUpProcessRequest.jsp?" + params.join("&"),
				beforeSend : function() {
					// Update Stats
					$('#status').html('Request Sent');
				},
				success : function(response) {

					var result = $.parseJSON(response).signUpValidationResult;
					console.log(result);
					if (result == "invalidName") {
						document.getElementById("invalidName").style.display = "block";

						document.getElementById("signUpSuccess").style.display = "none";
						document.getElementById("signUpFailure").style.display = "none";
					} else if (result == "signUpSuccess") {
						document.getElementById("signUpSuccess").style.display = "block";

						document.getElementById("invalidName").style.display = "none";
						document.getElementById("signUpFailure").style.display = "none";
					} else if (result == "signUpFailure") {
						document.getElementById("signUpFailure").style.display = "block";

						document.getElementById("invalidName").style.display = "none";
						document.getElementById("signUpSuccess").style.display = "none";
					} else {
						document.getElementById("invalidName").style.display = "none";
						document.getElementById("signUpSuccess").style.display = "none";
						document.getElementById("signUpFailure").style.display = "none";
					}
				},
				error : function() {
					// Failed request
					$('#status').html('Oops! Error.');
				}
			});
}