/* new code */
function analytics_refresh() {
	console.log("analytics_refresh");
	var category = document.getElementById("category");
	var display = document.getElementsByClassName("display");
	var len = display.length;
	for (var i = 0; i < len; i++) {
		display[i].style.color = "black";
	}

	analytics_refresh_helper(category.value);
	console.log(category.value);
}

function analytics_refresh_helper(category) {
	$.ajax({
		type : 'POST',
		url : "jsp/analytics_refresh_helper.jsp?category=" + category,
		beforeSend : function() {
			// Update Stats
			$('#status').html('Request Sent');
		},
		success : function(response) {
			console.log("begin proccessing data");
			var result = $.parseJSON(response);
			for (var i = 0; i < result.length; ++i) {
				if (i == 0) {
					// overall
					var json = result[0];
					for (var j = 0; j < json.length; ++j) {
						var display = document.getElementById(json[j].key);
						console.log("overall: " + json[j].key);
						if (display != null) {
							display.style.color = "red";
							display.innerHTML = json[j].amount;
							console.log(json[j].amount);
						}
					}
				} else if (i == 1) {
					// states
					var json = result[1];
					for (var j = 0; j < json.length; ++j) {
						var display1 = document.getElementById(json[j].key
								+ ": statename");
						var display2 = document.getElementById(json[j].key + ": stateamount");
						console.log("state: " + json[j].key);
						console.log(json[j].amount);
						if (display1 != null) {
							display1.style.color = "red";
						}
						if (display2 != null) {
							display2.style.color = "red";
							display2.innerHTML = json[j].amount;
						}
					}

				} else if (i == 2) {
					// products
					var json = result[2];
					for (var j = 0; j < json.length; ++j) {
						var display1 = document.getElementById(json[j].key
								+ ": productname");
						var display2 = document.getElementById(json[j].key + ": productamount");
						console.log("product: " + json[j].key);
						console.log(json[j].amount);
						if (display1 != null) {
							display1.style.color = "red";
						}
						if (display2 != null) {
							display2.style.color = "red";
							display2.innerHTML = json[j].amount;
						}
					}
				}
			}
			console.log("done processing data");
		},
		error : function() {
			// Failed request
			console.log("error");
			$('#status').html('Oops! Error.');
		}
	});
}