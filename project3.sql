/* ----------------------------------- Index ----------------------------------- */
CREATE INDEX sales_uid ON sales(uid);
CREATE INDEX sales_pid ON sales(pid);
CREATE INDEX sales_updatetime ON sales(updatetime);
CREATE INDEX users_state ON users(state);
CREATE INDEX products_cid ON products(cid);
CREATE INDEX pre_states_cid ON pre_states_cid(stid);



drop table if exists pre_states;
drop table if exists pre_states_cid;
drop table if exists pre_products;
drop table if exists pre_states_products;
drop table if exists db_update_time;

create table pre_states(id serial primary key, stid integer, name text, amount integer, updateTime timestamp);
create table pre_states_cid(id serial primary key, stid integer, name text, cid integer, amount integer, updateTime timestamp);
create table pre_products(id serial primary key, pid integer, name text, cid integer, amount integer, updateTime timestamp);
create table pre_states_products(id serial primary key, stid integer, pid integer, amount integer, updateTime timestamp);
create table db_update_time(updateTime timestamp);


/* ----------------------------------- states table with amount each state spend in total ----------------------------------- */
insert into pre_states(stid, name, amount, updateTime)(select st.id, st.name, coalesce(sum(s.quantity*s.price), 0) as amt, now() 
	from states st left outer join users u on u.state = st.id left outer join sales s on s.uid = u.id 
	group by st.name, st.id 
	order by amt desc);


/* ----------------------------------- states table with amount each state spend in each category ----------------------------------- */
insert into pre_states_cid(stid, name, cid, amount, updateTime)
	(select x.stid, x.stname, x.cid, coalesce(sum(s.quantity*s.price),0), now()
		from (select st.id as stid, st.name as stname, c.id as cid from states st, categories c) x left outer join
		(select st2.id as st2id, c2.id as c2id, s2.quantity as quantity, s2.price as price 
			from states st2 left outer join users u on st2.id = u.state left outer join sales s2 on u.id = s2.uid 
			left outer join products p2 on p2.id = s2.pid left outer join categories c2 on p2.cid = c2.id) s 
		on x.stid = s.st2id and x.cid = s.c2id 
		group by x.stid, x.stname, x.cid);

/* ----------------------------------- product table with amount each product sold in total ----------------------------------- */
insert into pre_products(pid, name, cid, amount, updateTime)
	(select p.id, p.name, p.cid, coalesce(sum(s.quantity*s.price), 0) as amt, now() from products p left outer join sales s on s.pid = p.id 
		left outer join categories c on p.cid = c.id group by p.id, p.name, p.cid);

/* ----------------------------------- state and product mapping table to show how much a state spend on a product ----------------------------------- */
insert into pre_states_products(stid, pid, amount, updateTime)
	(select x.stid, x.pid, coalesce(sum(s.quantity*s.price),0), now() from 
		(select st.id as stid, p.id as pid, 0 as amt from states st, products p) x left outer join
		(select st2.id as st2id, p2.id as p2id, s2.quantity as quantity, s2.price as price from states st2 left outer join users u on
		st2.id = u.state left outer join sales s2 on u.id = s2.uid left outer join products p2 on p2.id = s2.pid) s 
		on x.stid = s.st2id and x.pid = s.p2id group by x.stid, x.pid, x.amt);
		
/* ----------------------------------- Add updatetime column to sales ----------------------------------- */
alter table sales add updateTime timestamp;
update sales set updateTime = now();

/* ----------------------------------- table that stores the last time the database is updated ----------------------------------- */
insert into db_update_time(updateTime)(select now());