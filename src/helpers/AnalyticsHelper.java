package helpers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.jsp.JspWriter;

import models.Analytics;
import models.Product;
import models.State;
import models.User;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class AnalyticsHelper {

	static final int show_num_row = 50, show_num_col = 50;

	public static Analytics AnalysisForUsers(String order, String category,
			int rowOffset, int colOffset, JspWriter out) {
		Connection conn = null;
		Statement stmt = null, stmt2 = null;
		ResultSet rs = null;
		// Statements that vary among user configurations
		String tmpTable1 = null, tmpTable2 = null;
		String userQuery = null, userQuery2 = null, userQuery3 = null;
		String productQuery = null, productQuery2 = null, productQuery3 = null;
		String colCount = null;
		int maxUser, maxProduct;

		// Statement that do not change
		String cellsQuery = "select s.uid, s.pid, coalesce(sum(s.quantity*s.price),0) from u_t u,p_t p, sales s where s.uid=u.id and s.pid=p.id group by s.uid, s.pid;";
		String rowCount = "select count(*) from users";

		try {
			conn = HelperUtils.connect();
			stmt = conn.createStatement();
			stmt2 = conn.createStatement();

			if ("Alphabetical".equals(order)) {
				tmpTable1 = "CREATE TEMP TABLE p_t (name text, id int);";
				tmpTable2 = "CREATE TEMP TABLE u_t (name text, id int);";
				if ("0".equals(category)) {
					userQuery = "select name, id from users order by name desc offset "
							+ rowOffset + " limit 20";
					userQuery2 = "insert into u_t (name, id) " + userQuery;
					userQuery3 = "select u.name, u.id, coalesce(sum(s.quantity*s.price),0) from  u_t u left outer join sales s on s.uid=u.id group by u.id, u.name order by u.name desc;";
					productQuery = "select name, id from products order by name desc offset "
							+ colOffset + " limit " + show_num_col;
					productQuery2 = "insert into p_t (name, id) "
							+ productQuery;
					productQuery3 = "select p.name, p.id, coalesce(sum(s.quantity*s.price),0) from p_t p left outer join sales s on s.pid=p.id  group by p.id, p.name order by p.name desc;";
					colCount = "select count(*) from products";
				} else {
					userQuery = "select name, id from users order by name desc offset "
							+ rowOffset + " limit 20";
					userQuery2 = "insert into u_t (name, id) " + userQuery;
					userQuery3 = "select u.name, u.id, coalesce(sum(s.quantity*s.price),0) from  u_t u left outer join (select s2.uid as uid, s2.pid as pid, s2.price as price, s2.quantity as quantity  from sales s2, products p where s2.pid = p.id and p.cid = "
							+ category
							+ ") s ON s.uid=u.id group by u.id, u.name;";
					productQuery = "select name, id from products where cid="
							+ category + " order by name desc offset "
							+ colOffset + " limit " + show_num_col;
					productQuery2 = "insert into p_t (name, id) "
							+ productQuery;
					productQuery3 = "select p.name, p.id, coalesce(sum(s.quantity*s.price),0) from p_t p left outer join sales s on s.pid=p.id  group by p.id, p.name order by p.name desc;";
					colCount = "select count(*) from products where cid ="
							+ category;
				}
			} else {
				tmpTable1 = "CREATE TEMP TABLE p_t (name text, id int, price int);";
				tmpTable2 = "CREATE TEMP TABLE u_t (name text, id int, price int);";
				if ("0".equals(category)) {
					userQuery = "select u.name, u.id, coalesce(sum(s.quantity*s.price),0) as price from  users u left outer join sales s on s.uid=u.id group by u.id, u.name order by price desc offset "
							+ rowOffset + " limit 20;";
					userQuery2 = "insert into u_t (name, id, price) "
							+ userQuery;
					userQuery3 = "select * from u_t;";
					productQuery = "select p.name, p.id, coalesce(sum(s.quantity*s.price),0) as price from products p left outer join sales s on s.pid=p.id  group by p.id, p.name order by price desc offset "
							+ colOffset + " limit 10;";
					productQuery2 = "insert into p_t (name, id, price) "
							+ productQuery;
					productQuery3 = "select * from p_t;";
					colCount = "select count(*) from products";
				} else {
					userQuery = "select u.name, u.id, coalesce(sum(s.quantity*s.price),0) as price from  users u left outer join (select s2.uid as uid, s2.pid as pid, s2.price as price, s2.quantity as quantity from sales s2, products p where s2.pid = p.id and p.cid = "
							+ category
							+ ") s ON s.uid=u.id group by u.id, u.name order by price desc offset "
							+ rowOffset + " limit 20;";
					userQuery2 = "insert into u_t (name, id, price) "
							+ userQuery;
					userQuery3 = "select * from u_t;";
					productQuery = "select p.name, p.id, coalesce(sum(s.quantity*s.price),0) as price from products p left outer join sales s ON s.pid=p.id WHERE p.cid="
							+ category
							+ " group by p.id, p.name order by price desc offset "
							+ colOffset + " limit 10;";
					productQuery2 = "insert into p_t (name, id, price) "
							+ productQuery;
					productQuery3 = "select * from p_t;";
					colCount = "select count(*) from products where cid ="
							+ category;
				}

			}
			// Dump on console query results;
			System.out.println("==========Query Dump (" + new Date()
					+ ")========");
			System.out.println("Temporary table 1 : " + tmpTable1);
			System.out.println("Temporary table 2 : " + tmpTable2);
			System.out.println("User Query 1 : " + userQuery);
			System.out.println("User Query 2 : " + userQuery2);
			System.out.println("User Query 3 : " + userQuery3);
			System.out.println("Product Query 1 : " + productQuery);
			System.out.println("Product Query 2 : " + productQuery2);
			System.out.println("Product Query 3 : " + productQuery3);
			System.out.println("Cells Query : " + cellsQuery);
			System.out.println("Row Count Query: " + rowCount);
			System.out.println("Column Count Query: " + colCount);

			long start = System.currentTimeMillis();
			// Create and fill temp tables
			stmt2.execute(tmpTable1);
			stmt2.execute(tmpTable2);
			stmt2.execute(userQuery2);
			stmt2.execute(productQuery2);

			// Get counts
			maxUser = getRowOrColumnCount(stmt, rowCount);
			maxProduct = getRowOrColumnCount(stmt, colCount);

			HashMap<Integer, User> users = new HashMap<Integer, User>();
			ArrayList<Integer> userPosition = new ArrayList<Integer>();
			HashMap<Integer, Integer> userPrices = new HashMap<Integer, Integer>();
			HashMap<Integer, Product> products = new HashMap<Integer, Product>();
			ArrayList<Integer> productPosition = new ArrayList<Integer>();
			HashMap<Integer, Integer> productPrices = new HashMap<Integer, Integer>();

			rs = stmt.executeQuery(userQuery3);
			while (rs.next()) {
				String name = rs.getString(1);
				int uid = rs.getInt(2);
				int amount = rs.getInt(3);
				userPrices.put(uid, amount);
				users.put(uid, new User(uid, name, null, 0, 0));
				userPosition.add(uid);
			}
			rs.close();

			rs = stmt.executeQuery(productQuery3);
			while (rs.next()) {
				String name = rs.getString(1);
				int pid = rs.getInt(2);
				int amount = rs.getInt(3);
				productPrices.put(pid, amount);
				products.put(pid, new Product(pid, 0, name, null, 0));
				productPosition.add(pid);
			}
			rs.close();
			int[][] prices = new int[show_num_row][show_num_col];
			rs = stmt.executeQuery(cellsQuery);
			while (rs.next()) {
				int uid = rs.getInt(1);
				int pid = rs.getInt(2);
				int amount = rs.getInt(3);
				prices[userPosition.indexOf(uid)][productPosition.indexOf(pid)] = amount;
			}
			rs.close();
			long end = System.currentTimeMillis();
			long time_taken = end - start;
			Analytics a = new Analytics(maxUser, maxProduct, 0, productPrices,
					userPrices, null, products, users, null, productPosition,
					userPosition, null, prices, time_taken);
			return a;
		} catch (Exception e) {
			printError(out, e);
			return new Analytics();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (stmt2 != null) {
					stmt2.close();
				}
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/* new code */
	public static Analytics AnalysisForStates(String order, String category,
			int rowOffset, int colOffset, JspWriter out) {
		Connection conn = null;
		Statement stmt = null, stmt2 = null;
		ResultSet rs = null;
		// Statements that vary among user configurations
		String tmpTable1 = null;
		String statesQuery = null;
		String productQuery = null, productQuery2 = null;
		String colCount = null;
		int maxState, maxProduct;

		// AnalyticsHelper.updateDB(out);

		// Statement that do not change
		String cellsQuery = "select sp.stid, sp.pid, sp.amount from pre_states_products sp, p_t p where sp.pid = p.id ;";
		String rowCount = "select count(*) from users";
		String timeQuery = "select * from db_update_time";

		try {
			conn = HelperUtils.connect();
			stmt = conn.createStatement();
			stmt2 = conn.createStatement();
			tmpTable1 = "CREATE TEMP TABLE p_t (name text, id int, amount int);";

			if ("0".equals(category)) {
				statesQuery = "select st.name, st.stid, amount from pre_states st order by amount desc limit 50;";
				productQuery = "select p.name, p.pid, amount from pre_products p order by amount desc limit 50;";
				colCount = "select count(*) from products";
			} else {
				statesQuery = "select st.name, st.stid, amount from pre_states_cid st where st.cid = "
						+ category + " order by amount desc limit 50;";
				productQuery = "select p.name, p.pid, amount from pre_products p where p.cid = "
						+ category + " order by amount desc limit 50;";
				colCount = "select count(*) from products where cid ="
						+ category;
			}

			productQuery2 = "insert into p_t (name, id, amount) "
					+ productQuery;

			// Dump on console query results;
			System.out.println("==========Query Dump (" + new Date()
					+ ")========");
			System.out.println("Temporary table 1 : " + tmpTable1);
			System.out.println("State Query 1 : " + statesQuery);
			System.out.println("Product Query 1 : " + productQuery);
			System.out.println("Product Query 2 : " + productQuery2);
			System.out.println("Cells Query : " + cellsQuery);
			System.out.println("Row Count Query: " + rowCount);
			System.out.println("Column Count Query: " + colCount);

			long start = System.currentTimeMillis();
			// Create and fill temp tables
			stmt2.execute(tmpTable1);
			stmt2.execute(productQuery2);

			// Get counts
			maxState = 50;
			maxProduct = getRowOrColumnCount(stmt, colCount);

			HashMap<Integer, State> states = new HashMap<Integer, State>();
			ArrayList<Integer> statePosition = new ArrayList<Integer>();
			HashMap<Integer, Integer> statePrices = new HashMap<Integer, Integer>();
			HashMap<Integer, Product> products = new HashMap<Integer, Product>();
			ArrayList<Integer> productPosition = new ArrayList<Integer>();
			HashMap<Integer, Integer> productPrices = new HashMap<Integer, Integer>();

			rs = stmt.executeQuery(statesQuery);
			while (rs.next()) {
				String name = rs.getString(1);
				int sid = rs.getInt(2);
				int amount = rs.getInt(3);
				states.put(sid, new State(sid, name));
				statePosition.add(sid);
				statePrices.put(sid, amount);
			}
			rs.close();

			rs = stmt.executeQuery(productQuery);
			while (rs.next()) {
				String name = rs.getString(1);
				int pid = rs.getInt(2);
				int amount = rs.getInt(3);
				productPrices.put(pid, amount);
				products.put(pid, new Product(pid, 0, name, null, 0));
				productPosition.add(pid);
			}
			rs.close();
			int[][] prices = new int[show_num_row][show_num_col];
			rs = stmt.executeQuery(cellsQuery);
			while (rs.next()) {
				int uid = rs.getInt(1);
				int pid = rs.getInt(2);
				int amount = rs.getInt(3);
				prices[statePosition.indexOf(uid)][productPosition.indexOf(pid)] = amount;
			}
			rs.close();

			String time = "";
			rs = stmt.executeQuery(timeQuery);
			while (rs.next()) {
				time = rs.getTimestamp(1).toString();
			}
			rs.close();

			long end = System.currentTimeMillis();
			long time_taken = end - start;
			Analytics a = new Analytics(0, maxProduct, maxState, productPrices,
					null, statePrices, products, null, states, productPosition,
					null, statePosition, prices, time_taken);
			a.setAccessDBTime(time);
			System.out.println("run time:" + time);
			return a;
		} catch (Exception e) {
			printError(out, e);
			return new Analytics();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (stmt2 != null) {
					stmt2.close();
				}
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/* new code */
	public static Pair<JSONArray, String> AnalysisForStatesRefresh(
			String updatetime, String category, JspWriter out) {
		System.out.println("-------updating---------");
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String statesQuery = "";
		String productsQuery = "";

		JSONArray resultset1 = new JSONArray();
		JSONArray resultset2 = new JSONArray();
		JSONArray resultset3 = new JSONArray();
		JSONArray resultset_final = new JSONArray();
		
		AnalyticsHelper.updateDB(out);

		System.out.println("time:" + updatetime);

		// Statement that do not change
		String cellsQuery = "select sp.stid, sp.pid, sp.amount from pre_states_products sp where sp.updatetime > '"
				+ updatetime + "';";
		String timeQuery = "select * from db_update_time";

		if ("0".equals(category)) {
			statesQuery = "select st.stid, amount from pre_states st where st.updatetime > '"
					+ updatetime + "';";
			productsQuery = "select p.pid, amount from pre_products p where p.updatetime > '"
					+ updatetime + "';";
		} else {
			statesQuery = "select st.stid, amount from pre_states_cid st where st.cid = "
					+ category + " and st.updatetime > '" + updatetime + "';";
			productsQuery = "select p.pid, amount from pre_products p where p.cid = "
					+ category + " and p.updatetime > '" + updatetime + "';";
		}

		try {
			conn = HelperUtils.connect();
			stmt = conn.createStatement();

			long start = System.currentTimeMillis();

			rs = stmt.executeQuery(cellsQuery);
			while (rs.next()) {
				int stid = rs.getInt(1);
				int pid = rs.getInt(2);
				int amount = rs.getInt(3);
				System.out.println("1: " + stid);
				JSONObject json = new JSONObject();
				json.put("key", stid + ", " + pid);
				json.put("amount", amount);
				System.out.println(json);
				resultset1.add(json);
				// result.add(new Pair<String, Integer>(stid + ", " + pid,
				// amount));
			}
			rs.close();

			rs = stmt.executeQuery(statesQuery);
			while (rs.next()) {
				int stid = rs.getInt(1);
				int amount = rs.getInt(2);
				System.out.println("2: " + stid);
				JSONObject json = new JSONObject();
				json.put("key", stid);
				json.put("amount", amount);
				System.out.println(json);
				resultset2.add(json);
			}
			rs.close();

			rs = stmt.executeQuery(productsQuery);
			while (rs.next()) {
				int pid = rs.getInt(1);
				int amount = rs.getInt(2);
				JSONObject json = new JSONObject();
				json.put("key", pid);
				json.put("amount", amount);
				System.out.println(json);
				resultset3.add(json);
			}
			rs.close();

			resultset_final.add(resultset1);
			resultset_final.add(resultset2);
			resultset_final.add(resultset3);

			String newUpdatetime = "";
			rs = stmt.executeQuery(timeQuery);
			while (rs.next()) {
				newUpdatetime = rs.getTimestamp(1).toString();
				System.out.println("updateTime:" + newUpdatetime);
			}
			rs.close();

			return new Pair<JSONArray, String>(resultset_final, newUpdatetime);
		} catch (Exception e) {
			printError(out, e);
			return new Pair<JSONArray, String>(new JSONArray(), new String());
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/* new code */
	public static void updateDB(JspWriter out) {
		Connection conn = null;
		Statement stmt = null, stmt2 = null;
		ResultSet rs = null;

		// Statement that do not change
		String salesData = "select u.state, s.pid, p.cid, (s.quantity * s.price) as amt, s.updatetime from sales s, users u, products p, "
				+ "db_update_time db where s.uid = u.id and s.pid = p.id and s.updatetime > db.updatetime;";

		try {
			conn = HelperUtils.connect();
			stmt = conn.createStatement();
			stmt2 = conn.createStatement();

			long start = System.currentTimeMillis();

			rs = stmt.executeQuery(salesData);
			
			System.out.println("done running salesupdate");
			
			while (rs.next()) {
				int stid = rs.getInt(1);
				int pid = rs.getInt(2);
				int cid = rs.getInt(3);
				int newAmt = rs.getInt(4);
				String updateTime = rs.getTimestamp(5).toString();

				System.out.println(updateTime);
				stmt2.executeUpdate("update pre_states set amount = amount + "
						+ newAmt + ", updatetime = now() where stid = " + stid
						+ " and updatetime < '" + updateTime + "';");

				stmt2.executeUpdate("update pre_states_cid set amount = amount + "
						+ newAmt
						+ ", updatetime = now() where stid = "
						+ stid
						+ " and cid = "
						+ cid
						+ " and updatetime < '"
						+ updateTime + "';");

				stmt2.executeUpdate("update pre_products set amount = amount + "
						+ newAmt
						+ ", updatetime = now() where pid = "
						+ pid
						+ " and updatetime < '" + updateTime + "';");

				stmt2.executeUpdate("update pre_states_products set amount = amount + "
						+ newAmt
						+ ", updatetime = now() where stid = "
						+ stid
						+ " and pid = "
						+ pid
						+ " and updatetime < '"
						+ updateTime + "';");
			}
			rs.close();

			stmt2.executeUpdate("update db_update_time set updatetime = now()");

			long end = System.currentTimeMillis();
			long time_taken = end - start;
			System.out.println("update time taken: " + time_taken);
		} catch (Exception e) {
			printError(out, e);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (stmt2 != null) {
					stmt2.close();
				}
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	private static void printError(JspWriter out, Exception e) {
		try {
			out.println(HelperUtils
					.printError("Internal Server Error. This shouldn't happen : "
							+ e.getMessage()));
			e.printStackTrace();
		} catch (Exception e2) {
			System.out.println(e2.getLocalizedMessage());
		}
	}

	private static int getRowOrColumnCount(Statement stmt, String query)
			throws SQLException {
		ResultSet rs = stmt.executeQuery(query);
		if (rs.next()) {
			return rs.getInt(1);
		}
		throw new SQLException("There was an error processing query " + query);
	}

	public static String isSelected(String option, String selected) {
		if (option.equals(selected)) {
			return "<option value=\"" + option + "\" selected=\"selected\">"
					+ option + "</option>";
		} else {
			return "<option value=\"" + option + "\">" + option + "</option>";
		}
	}

	public static String isSelected(int value, String option, String selected) {
		int selectedValue = -1;
		try {
			selectedValue = Integer.parseInt(selected);
		} catch (NumberFormatException e) {
		}
		if (value == selectedValue) {
			return "<option value=\"" + value + "\" selected=\"selected\">"
					+ option + "</option>";
		} else {
			return "<option value=\"" + value + "\">" + option + "</option>";
		}
	}
}
